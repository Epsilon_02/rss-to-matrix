# -*- coding: utf-8 -*-

# Description:
"""
This script parse some rss-feeds. They should be given in the "url-list". After parsing the url,
they would be sent with matrix. Feedback and comments are highly appreciated.
"""

# Date: 29.11.2021
# Revision: 17.06.2023
__author__ = "code@schoeneberge.eu"
__version__ = "0.0.4"


import os
import datetime
import hashlib
import sqlite3
import asyncio
import requests
from requests.exceptions import RequestException, HTTPError, Timeout, TooManyRedirects
from bs4 import BeautifulSoup
import yaml
from nio import AsyncClient

CONF_FOLDER_PATH = f"{os.getenv('HOME')}/.config/rssfeeder"
CONF_FILE_NAME = "config.yaml"
CONF_FILE_PATH = os.path.join(CONF_FOLDER_PATH, CONF_FILE_NAME)

with open(CONF_FILE_PATH, 'r', encoding='UTF-8') as myconf:
    cfg = yaml.load(myconf, Loader=yaml.FullLoader)

database = cfg['DATABASES']['RSS']
DOMAIN = cfg['FQDN_DOMAINS']['MATRIX']
USER = cfg['USER']['MATRIX']
PASSWD = cfg['PASS']['MATRIX']
ROOM = cfg['MATRIX_ROOMS']['RSS']

def get_data(corresponding_database, col_name):
    """
    This function returns the respective value on a sql query. Possible values are
    title, texthash, pubDate and sendDate
    """
    return_file = []
    conn = sqlite3.connect(corresponding_database)
    cursor = conn.execute("SELECT id, texthash, published_date, send_date from MYRSSTABLE")
    if col_name == "id":
        for row in cursor:
            return row[0]
    elif col_name == "texthash":
        for row in cursor:
            return_file.append(row[1])
    elif col_name == "published":
        for row in cursor:
            return row[2]
    elif col_name == "senddate":
        for row in cursor:
            return row[3]
    conn.close()
    return return_file

def check_titles(corresponding_database, texthash) -> bool:
    """
    This function checks if a passed text hash already exists in the database. If yes,
    a false is returned, if not, then true.
    """
    existing_titles = get_data(corresponding_database, "texthash")
    if not texthash in existing_titles:
        return True
    else:
        return False

def get_rss(url):
    """
    This function parses a page and looks for the title, the pubDate, the link and, if
    available, a description and/or an abstract.
    """
    try:
        response = requests.get(url, timeout=5)
        send_file = ""
        soup = BeautifulSoup(response.content, features='xml')
        articles = soup.findAll('item')

# Parse over the site and find title, link, pub-date and, if there is one, abstract
        for item in articles:
            title = item.find('title').text
            link = item.find('link').text
            published = item.find('pubDate').text
            try:
                abstract = item.find('abstract').text
                texthash_for_sql = hashlib.md5(abstract.encode('UTF-8')).hexdigest()
            except AttributeError:
                texthash_for_sql = None
            try:
                description = item.find('description').text
                texthash_for_sql = hashlib.md5(description.encode('UTF-8')).hexdigest()
            except AttributeError:
                texthash_for_sql = None

            if texthash_for_sql is None:
                texthash_for_sql = hashlib.md5(title.encode('UTF-8')).hexdigest()


# Set Send-Time
            now = datetime.datetime.now()
# The format should be e. g.: Tue, 13 Jun 2023 20:30:40 +0200
            time_now = now.strftime('%a, %d %b %Y %H:%M:%S %z')

# Check if database existing
            if not os.path.isfile(database):
                conn = sqlite3.connect(database)
                print("Opened database successfully")
                conn.execute('''CREATE TABLE MYRSSTABLE
                    (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    TEXTHASH TEXT NOT NULL,
                    PUBLISHED_DATE TEXT NOT NULL,
                    SEND_DATE TEXT NOT NULL);''')
                print("Table created successfully")
                conn.close()

# Check if title already exists
            if check_titles(database, texthash_for_sql):
                conn = sqlite3.connect(database)
                conn.execute("INSERT INTO MYRSSTABLE (TEXTHASH, PUBLISHED_DATE, SEND_DATE)\
                    VALUES (?, ?, ?);", (texthash_for_sql, published, time_now))
                conn.commit()
                print("Content inserted in database")
                conn.close()

# Create and fill the send_file
                send_file = f"{title}\n"
                if abstract:
                    send_file += f"{abstract}\n"
                if description:
                    send_file += f"{description}\n"
                send_file += f"{link}\n\n"

                # cleantext = BeautifulSoup(send_file, "lxml").text

# Send files via matrix
        if send_file != "":
            async def main():
                client = AsyncClient(DOMAIN, USER)

                await client.login(PASSWD)
                await client.room_send(
                    room_id= ROOM,
                    message_type="m.room.message",
                    content={
                        "msgtype": "m.text",
                        "body": send_file
                    }
                )
                await client.close()

            asyncio.run(main())

        else:
            print("There is no content to send")

    except ConnectionError as conn_error:
        print("Connection error: ", conn_error)
    except HTTPError as http_error:
        print("HTTP error: ", http_error)
    except Timeout as timeout_error:
        print("Timeout error: ", timeout_error)
    except TooManyRedirects as tmr_error:
        print("There are too many redirects: ", tmr_error)
    except RequestException as req_exc_error:
        print("Error while request: ", req_exc_error)

if __name__ == "__main__":
    for page in cfg['URLS']['FIRST']:
        get_rss(page)
