#!/usr/bin/env python3
# -*- coding: utf8 -*-

__author__ = "code@schoeneberge.eu"
__version__ = "1.0.1"
__date__ = "04.12.2021"

import sys
import sqlite3
import yaml
import getopt
import confighelper

# confighelper.check_configfile(confighelper.rssConfig)

# with open(confighelper.rssConfig, 'r') as myconf:
with open("/home/ahab/Programs/rssfeeder/pequodconf.yaml") as myconf:
    cfg = yaml.load(myconf, Loader=yaml.FullLoader)

# Create database:
def create_table(database):
    conn = sqlite3.connect(database)
    print("Opened database successfully")

    conn.execute('''CREATE TABLE RSS
            (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            TITLE   TEXT    NOT NULL,
            PUBLISHED_DATE  TEXT    NOT NULL,
            SEND_DATE   TEXT);''')
    print("Table created successfully", database)

    conn.close()

# Insert some data:
def insert_data(database, title, pubdate, senddate):
    conn = sqlite3.connect(database)
    conn.execute("INSERT INTO RSS (TITLE, PUBLISHED_DATE, SEND_DATE) VALUES (?, ?, ?);", (title, pubdate, senddate))
    conn.commit()
    print("Records created successfully")
    conn.close()

def show_data(database, var1):
    conn = sqlite3.connect(database)
    cursor = conn.execute("SELECT id, title, published_date, send_date from RSS")
    if var1 == "id":
        for row in cursor:
            print("ID = ", row[0])
    elif var1 == "title":
        for row in cursor:
            print("TITLE = ", row[1])
    elif var1 == "pubdate":
        for row in cursor:
            print("PUB-DATE = ", row[2])
    elif var1 == "senddate":
        for row in cursor:
            print("Send-Date = ", row[3], "\n")
    elif var1 == "all":
        for row in cursor:
            print("ID = ", row[0])
            print("TITLE = ", row[1])
            print("PUB-DATE = ", row[2])
            print("Send-Date = ", row[3], "\n")
    print("Operation done successfully")
    conn.close()

def get_data(database, var1):
    return_file = []
    conn = sqlite3.connect(database)
    cursor = conn.execute("SELECT id, title, published_date, send_date from rss")
    if var1 == "id":
        for row in cursor:
            return row[0]
    elif var1 == "title":
        for row in cursor:
            return_file.append(row[1])
    elif var1 == "pubdate":
        for row in cursor:
            return row[2]
    elif var1 == "senddate":
        for row in cursor:
            return row[3]
    conn.close()
    return return_file

def update_data(database):
    conn = sqlite3.connect(database)
    conn.execute("UPDATE RSS set title = testtitel where ID = 1;")
    conn.commit()
    print("Total number of rows updated :", conn.total_changes)

def delete_data(database, value):
    conn = sqlite3.connect(database)
    sql = 'DELETE FROM RSS WHERE PUBLISHED_DATE=?'
    cur = conn.cursor()
    cur.execute(sql, (value,))
    conn.commit()
    print("Total number of rows deleted :", conn.total_changes)

def main(argv):
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hdcis:", ["help", "show", "delete", "create", "insert" "database=", "optionsvar=", "title=", "pubdate=", "senddate="])
    except getopt.GetoptError:
        print("mysql -h\nmysql -d <value>\nmysql -c <database>\nmysql -i <value>")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(__doc__)
            sys.exit()
        elif opt in ("-s", "--show"):
            database = arg
            show_data(database, "all")
        elif opt in ("-d", "--deletevar"):
            optionsvar = arg
            database = arg
            delete_data(database, optionsvar)
        elif opt in ("-c", "--create"):
            optionsvar = arg
        elif opt in ("-i", "--insert"):
            optionsvar = arg
            database = arg
            title = arg
            pubdate = arg
            senddate = arg
            insert_data(database, title, pubdate, senddate)
        else:
            print("No valid option found")

if __name__ == "__main__":
    main(sys.argv[1:])
    database = "rssDatabase.db"
    show_data(database, "all")
    #delete_data(database, PUB-DATA, "Mon, 15 Nov 2021 13:52:20 +0100")
    # get_data(database, "title")
