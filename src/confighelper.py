# -*- coding: utf-8 -*-

# Description:
"""
This script initializes the config. If none exists yet, one is built and 
stored under ~/rssfeeder/rssconf.yaml. For this, some details like the 
user for matrix and the domain are queried. This script should be a helper 
script to assist the main script.
"""

# Date: 23.01.2022
# Revision: 17.06.2022
__author__ = "code@schoeneberge.eu"
__version__ = "0.0.5"


import os
import sys
import re
from pathlib import Path
import yaml

# Declare global variables
# MATRIX_USER = None
MATRIX_PASS = None
# MATRIX_DOMAIN = None
MATRIX_DOMAIN_QUOTED = None
# MATRIX_ROOM = None
FIRST_SITE = None

CONF_FOLDER_PATH = f"{os.getenv('HOME')}/.config/rssfeeder"
CONF_FILE_NAME = "config.yaml"
CONF_FILE_PATH = os.path.join(CONF_FOLDER_PATH, CONF_FILE_NAME)

DATABASE_PATH = "/var/lib/rssfeeder"
DATABASE_FILE = "rssDatabase.db"
DATABASE_FILE_PATH = os.path.join(DATABASE_PATH, DATABASE_FILE)

def check_configfile(check_file):
    """
    Checks if a config already exists. If not, some functions were .
    """
    if not os.path.isfile(check_file):
        if ask_new_config():
            # data_input()
            create_default_config()

# Ask if a new config should be created
def ask_new_config() -> bool:
    """
    Ask if a new config should be generated.
    """
    print("No config could be found.\n")
    while True:
        answer = input("New config under $HOME/.config/rssfeeder/conf.yaml? [yes|no|quit] ?\n")
        if answer.lower() in ['yes', 'y']:
            return True
        elif answer.lower() in ['no', 'n']:
            return False
        elif answer.lower() in ['exit', 'quit', 'q']:
            sys.exit()
        else:
            print("Sorry, that was not a valid answer. Restart")

# Query the required data:
def data_input():
    """
    Some questions are asked to create a config. Check the user input for valid entries
    with some regex.
    """
#     global MATRIX_USER, MATRIX_PASS, MATRIX_DOMAIN, MATRIX_DOMAIN_QUOTED, MATRIX_ROOM, FIRST_SITE
#     MATRIX_DOMAIN = input("Enter the name of your matrix-homeserver domain:\n")
#     domain_check = re.search(r'(^.+)(\.)(.){2,5}($)', MATRIX_DOMAIN)
#     if domain_check is None:
#         MATRIX_DOMAIN = input("This seems not like a valid domain, please use\
#             this schema: domain.tld : \n")
#     MATRIX_USER = input("Enter the name of the user from whom the messages should be sent:\n")
#     user_check = re.search(r'(^@)(.+)(:)(.+)(\.)(.){2,5}($)', MATRIX_USER)
#     if user_check is None:
#         MATRIX_USER = input("This seems not like a valid matrix username, please use\
#             this schema: @user:domain.tld : \n")
    # MATRIX_PASS = input("Please enter the password for the user:\n")
#     MATRIX_ROOM = input("Enter the room ID where the messages should be received:\n")
#     room_check = re.search(r'(^!.+)(:)(.+)(\.)(.){2,5}($)', MATRIX_ROOM)
#     if room_check is None:
#         MATRIX_ROOM = input("This seems not like a valid matrix room, please\
#             use this schema: !roomID:domain.tld : \n")
    # FIRST_SITE = "http://rss.sueddeutsche.de/rss/Wissen"

def domain_input() -> str:
    domain_check = None
    while domain_check is None:
        matrix_domain = input("Enter the name of your matrix-homeserver domain:\n")
        domain_check = re.search(r'(^.+)(\.)(.){2,5}($)', matrix_domain)
        if domain_check is None:
            matrix_domain = input("This seems not like a valid domain, please use\
                this schema: domain.tld : \n")
    return matrix_domain

def user_input() -> str:
    user_check = None
    while user_check is None:
        matrix_user = input("Enter the name of the user from whom the messages should be sent:\n")
        user_check = re.search(r'(^@)(.+)(:)(.+)(\.)(.){2,5}($)', matrix_user)
        if user_check is None:
            matrix_user = input("This seems not like a valid matrix username, please use\
                this schema: @user:domain.tld : \n")
    return matrix_user

def pass_input() -> str:
    return input("Please enter the password for the user:\n")


def room_input() -> str:
    room_check = None
    while room_check is None:
        matrix_room = input("Enter the room ID where the messages should be received:\n")
    room_check = re.search(r'(^!.+)(:)(.+)(\.)(.){2,5}($)', matrix_room)
    if room_check is None:
        matrix_room = input("This seems not like a valid matrix room, please\
            use this schema: !roomID:domain.tld : \n")
    return matrix_room

def site_input() -> str:
    return "http://rss.sueddeutsche.de/rss/Wissen"

def create_default_config():
    """
    Here config.yaml is created. For this, a yaml is built and
    filled with the data requested above (ask_new_config()).
    """
# Defines a few given variables
    fqdn_domain = "https://" + domain_input()

# Writes the variables into a dict
    dictfile = {
        'DOMAINS': {
            'MATRIX': MATRIX_DOMAIN_QUOTED
        },
        'FQDN_DOMAINS': {
            'MATRIX': 'https://' + fqdn_domain
        },
        'USER': {
            'MATRIX': user_input()
        },
        'PASS': {
            'MATRIX': pass_input()
        },
        'MATRIX_ROOMS': {
            'RSS': room_input()
        },
        'DATABASES': {
            'RSS': DATABASE_FILE_PATH
        },
        'URLS': {
            'FIRST': [site_input()]
        }
    }
# Creates the folder under ~/.config/, if it does not exist yet
    if not os.path.exists(CONF_FOLDER_PATH):
        Path(CONF_FOLDER_PATH).mkdir(mode=0o700, parents=True, exist_ok=True)
# Then write the dict into the specified file if the config does not exist yet
    if os.path.exists(CONF_FOLDER_PATH):
        with open(CONF_FILE_PATH, 'w', encoding='UTF-8') as file:
            yaml.dump(dictfile, file, default_flow_style=False, sort_keys=False)

def make_db_folder():
    """
    If the database folder /var/lib/rssfeeder not exists, then create it
    """
    if not os.path.exists(DATABASE_FILE_PATH):
        Path(DATABASE_PATH).mkdir(parents=True, exist_ok=True)

check_configfile(CONF_FILE_PATH)
make_db_folder()

# Move the GetContent file to /usr/local/sbin/
USR_LOCAL_SBIN_PATH = "/usr/local/sbin/"
INIT_FOLDER_NAME = os.path.dirname(__file__)
INIT_GETCONTENT_PATH = INIT_FOLDER_NAME + "/GetContent.py"
NEW_GETCONTENT_PATH = os.path.join(USR_LOCAL_SBIN_PATH, "GetContent.py")
os.rename(INIT_GETCONTENT_PATH, NEW_GETCONTENT_PATH)
